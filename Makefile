NAME=mart0n/exiv2
REGISTRY=docker.io
TAG=latest

DOCKER=docker

build:
	$(DOCKER) build --rm --pull -t $(REGISTRY)/$(NAME):$(TAG) .

push:
	$(DOCKER) push $(REGISTRY)/$(NAME):$(TAG)
